#!/bin/bash

git submodule init
git submodule update

export MKLROOT=/opt/mkl/2019.01/mkl
export N=4000

MKL_STATIC="-Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lgomp -lpthread -lm -ldl  -DMKL_ILP64 -m64 -I${MKLROOT}/include"
MKL_DYNMAIC="-L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl  -DMKL_ILP64 -m64 -I${MKLROOT}/include"

g++ -DN_elem=$N main_eigen.cpp -std=c++17 -o main_mkl -O3 -march=native -fopenmp -I./eigen -DEIGEN_USE_MKL_ALL -DMKL_DIRECT_CALL ${MKL_STATIC}
g++ -DN_elem=$N main_eigen.cpp -std=c++17 -o main_eigen -O3 -march=native -fopenmp -I./eigen
g++ -DN_elem=$N main.cpp -std=c++17 -o main -O3 -march=native -fopenmp


export LD_LIBRARY_PATH=${MKLROOT}/lib/intel64
export OMP_NUM_THREADS=8
export GOMP_CPU_AFFINITY="0-7" 
export OMP_WAIT_POLICY=active
#export MKL_NUM_THREADS=8 # vergl. https://software.intel.com/en-us/articles/recommended-settings-for-calling-intel-mkl-routines-from-multi-threaded-applications
#export MKL_DOMAIN_NUM_THREADS=8 # vergl. https://software.intel.com/en-us/mkl-linux-developer-guide-mkl-domain-num-threads
#export MKL_DYNAMIC=true # vergl. https://software.intel.com/en-us/articles/recommended-settings-for-calling-intel-mkl-routines-from-multi-threaded-applications
#export MKL_DEBUG_CPU_TYPE=5 #Intel HSW (AVX256), vegl. http://public.clu2.fastmail.us/icc_cpu_dispatch.html 
#export MKL_DEBUG_CPU_MA=0i # vegl. http://public.clu2.fastmail.us/icc_cpu_dispatch.html 

echo "./main_mkl"
for i in {1..10}
do
    ./main_mkl
done

echo "./main_eigen"
for i in {1..10}
do
    ./main_eigen
done

echo "./main"
for i in {1..10}
do
    ./main
done
