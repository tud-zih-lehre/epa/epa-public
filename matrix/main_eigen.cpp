#include <iostream>
#include <vector>
#include <chrono>
#include <Eigen/Dense>

#ifndef N_elem
#define N_elem 2000
#endif

int main()
{
    int n = N_elem;
    Eigen::MatrixXd a(n,n);

    Eigen::MatrixXd b(n,n);
    
    Eigen::MatrixXd c(n,n);
        
    auto tp1 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i<n*n; i++)
    {
        a(i/n, i%n) = static_cast<double>(i);
        b(i/n, i%n) = static_cast<double>(i);
        c(i/n, i%n) = 0;
    }
    auto tp2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = tp2 - tp1;
//     std::cout << "time: " << diff.count() << "s" << std::endl;
    
    tp1 = std::chrono::high_resolution_clock::now();
    c = a * b;
    tp2 = std::chrono::high_resolution_clock::now();
    diff = tp2 - tp1;
    std::cout << "time: " << diff.count() << "s" << std::endl;

    
//     std::cout << "a: -------\n" ;
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << a(i,j) << " ";
//         }
//         std::cout << "\n";
//     }
//     
//     std::cout << "b: -------\n";
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << b(i,j) << " ";
//         }
//         std::cout << "\n";
//     }
// 
//     std::cout << "c: -------\n";
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << c(i,j) << " ";
//         }
//         std::cout << "\n";
//     }

}
