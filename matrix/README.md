# How To

Um das Beispiel ./compile_and_run.sh laufen lassen zu können müssen sie sich zuerst die Intel MKL installieren:

https://software.intel.com/en-us/mkl

Da nach müssen sie die Environment Variablen `MKLROOT`, `OMP_NUM_THREADS` und `GOMP_CPU_AFFINITY` an ihr System anpassen.
