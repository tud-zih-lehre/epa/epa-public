#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <omp.h>

#ifndef N_elem
#define N_elem 2000
#endif

int main()
{
    int n = N_elem;
    std::vector<double> a;
    a.reserve(n*n); 

    std::vector<double> b;
    b.reserve(n*n);
    
    std::vector<double> c;
    c.reserve(n*n);
    
// #pragma omp parallel
//     {
//         if (omp_get_thread_num()==0)
//             std::cout << "threads: " << omp_get_num_threads() << std::endl;
//     }
    
    auto tp1 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i<n*n; i++)
    {
        a[i] = static_cast<double>(i);
        b[i] = static_cast<double>(i);
        c[i] = 0;
    }
    auto tp2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = tp2 - tp1;
//     std::cout << "time: " << diff.count() << "s" << std::endl;
    
    tp1 = std::chrono::high_resolution_clock::now();
    

#pragma omp parallel for
    for (int i=0; i<n; i++)
    {
        auto in = i*n; 
        for (int k=0; k<n; k++)
        {
            auto kn = k*n;
            auto aink = a[in+k];
            for (int j=0; j<n; j++)
            {
                c[in+j] += aink * b[kn+j];
            }
        }
    }
    tp2 = std::chrono::high_resolution_clock::now();
    diff = tp2 - tp1;
    std::cout << "time: " << diff.count() << "s" << std::endl;

    
//     std::cout << "a: -------\n" ;
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << a[i*n+j] << " ";
//         }
//         std::cout << "\n";
//     }
//     
//     std::cout << "b: -------\n";
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << b[i*n+j] << " ";
//         }
//         std::cout << "\n";
//     }
// 
//     std::cout << "c: -------\n";
//     for (int i = 0; i<n ; i++)
//     {
//         for (int j = 0; j<n; j++)
//         {
//             std::cout << c[i*n+j] << " ";
//         }
//         std::cout << "\n";
//     }

}
