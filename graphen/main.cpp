/*
 * main.cpp
 *
 *  Created on: 17.01.2019
 *      Author: gocht
 */
#include "epa_graphs.hpp"

#include <iostream>

void print_graph(const epa::graph::graph_t &graph)
{
    for (const auto &line : graph)
    {
        std::cout << line.first << ": [";
        for (const auto &elem : line.second)
        {
            std::cout << "(" << std::get<0>(elem) << ", " << std::get<1>(elem) << ")";
        }
        std::cout << "]" << std::endl;
    }
}

int main()
{
    std::cout << "gen_directed_graph" << std::endl;
    {
        auto graph = epa::graph::gen_directed_graph(10, 2);
        print_graph(graph);
    }
    std::cout << "gen_directed_graph_rand" << std::endl;
    {
        auto graph = epa::graph::gen_directed_graph_rand(10, 2);
        print_graph(graph);
    }
    std::cout << "gen_undirected_graph" << std::endl;
    {
        auto graph = epa::graph::gen_undirected_graph(10, 2);
        print_graph(graph);
    }
    std::cout << "gen_undirected_graph_rand" << std::endl;
    {
        auto graph = epa::graph::gen_undirected_graph_rand(10, 2);
        print_graph(graph);
    }
    std::cout << "Bye Bye" << std::endl;
}
