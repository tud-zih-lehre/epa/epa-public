/*
 * epa_numbers.hpp
 *
 *  Created on: 11.01.2019
 *      Author: gocht
 */

#ifndef SORTIEREN_EPA_NUMBERS_HPP_
#define SORTIEREN_EPA_NUMBERS_HPP_

#include <list>
#include <random>
#include <vector>

namespace epa
{
namespace number
{
/*generates a list with n elements
 *
 *@param n element in the list
 *@return list with n elements
 */
inline std::vector<double> generate_case_1(size_t n = 100)
{
    std::vector<double> data(n, 0);
    for (uint i = 0; i < n; i++)
    {
        data[i] = static_cast<double>(n - i);
    }
    return data;
}

/*generates a list with n elements
 *
 *@param n element in the list
 *@return list with n elements
 */
inline std::vector<double> generate_case_2(size_t n = 100)
{
    std::vector<double> data(n, 0);
    for (uint i = 0; i < n; i++)
    {
        auto pivot = n / 2;
        if (i < pivot)
        {
            data[i] = static_cast<double>(i);
        }
        else
        {
            data[i] = static_cast<double>(n - i);
        }
    }
    return data;
}

/*generates a list with n elements
 *
 *@param n element in the list
 *@param max_val maximum value
 *
 *@return list with n elements
 */
inline std::vector<double> generate_case_3(size_t n = 100, double max_val = 1000)
{
    std::vector<double> data(n, 0);
    std::random_device rd;
    std::uniform_real_distribution<double> dist(0, max_val);

    for (auto &elem : data)
    {
        elem = dist(rd);
    }
    return data;
}

/*generates a list with n elements
 *
 *@param n element in the list
 *@param max_val maximum value
 *
 *@return list with n elements
 */
inline std::vector<double> generate_case_5(size_t n = 100)
{
    std::vector<double> data(n, 0);
    for (uint i = 0; i < n; i++)
    {
        data[i] = static_cast<double>(i);
    }
    return data;
}

/* generates a list with n elements
 *
 * @param n element in the list
 * @param low lower bound for elements
 * @param high higher bound for elements
 *
 * @return list with n elements
 */
inline std::list<double> generate_case_5_helper(double low, double high, size_t n)
{
    if (n == 0)
    {
        return std::list<double>();
    }
    else
    {
        auto priv = high - 1;

        if (n % 2 == 1)
        {
            auto left = generate_case_5_helper(low, priv, n / 2);
            auto right = generate_case_5_helper(low, priv, n / 2);

            left.push_back(priv);
            left.splice(left.end(), right);
            return left;
        }
        else
        {
            auto left = generate_case_5_helper(low, priv, n / 2 - 1);
            auto right = generate_case_5_helper(low, priv, n / 2);

            left.push_back(priv);
            left.splice(left.end(), right);
            return left;
        }
    }
}

inline std::list<double> generate_case_4(size_t n = 100)
{
    return generate_case_5_helper(0, n, n);
}
} // namespace number
} // namespace epa

#endif /* SORTIEREN_EPA_NUMBERS_HPP_ */
