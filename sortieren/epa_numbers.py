import random
import numpy as np

def generate_case_1(n = 100):
    """generates a list with n elements
    
    @param n element in the list
    @return list with n elements
    """
    data = np.zeros(n)
    for i in range(n):
        data[i]=n-i
    return data

def generate_case_2(n = 100):
    """generates a list with n elements
    
    @param n element in the list
    @return list with n elements
    """
    pivot = n//2
    data = np.zeros(n)
    for i in range(n):
        if i < pivot:
            data[i]=i
        else:
            data[i]=n-i
    return data

def generate_case_3(n = 100, max_val = 1000):
    """generates a list with n elements
    
    @param n element in the list
    @param max_val maximum value
    @return list with n elements
    """
    data = np.zeros(n)
    for i in range(n):
        data[i]=random.uniform(0,max_val)
    return data

def generate_case_4(n = 100):
    """generates a list with n elements
    
    @param n element in the list
    @return list with n elements
    """
    data = np.zeros(n)
    for i in range(n):
        data[i]=i
    return data

def generate_case_5_helper(low, high, n):
    """generates a list with n elements
    
    @param n element in the list
    @param low lower bound for elements
    @param high higher bound for elements
    @return list with n elements
    """
    if(n == 0):
        return []
    else:
        priv = high -1
        if n%2 == 1:
            left = generate_case_5_helper(low, priv ,n//2)
            right = generate_case_5_helper(low, priv, n//2)
            return left + [priv] + right
        else:
            left = generate_case_5_helper(low, priv, n//2-1)
            right = generate_case_5_helper(low, priv, n//2)
            return left + [priv] + right
            
def generate_case_5(n = 100):
    """generates a list with n elements
    
    @param n element in the list
    @return list with n elements
    """
    data = generate_case_5_helper(0,n,n)
    return np.asarray(data)
