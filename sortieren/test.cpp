/*
 * test.cpp
 *
 *  Created on: 11.01.2019
 *      Author: gocht
 */

#include "epa_numbers.hpp"
#include <iostream>

int main()
{
    {
        auto data = epa::number::generate_case_1(10);

        for (auto &elem : data)
        {
            std::cout << elem << ",";
        }
        std::cout << std::endl;
    }
    {
        auto data = epa::number::generate_case_2(10);

        for (auto &elem : data)
        {
            std::cout << elem << ",";
        }
        std::cout << std::endl;
    }
    {
        auto data = epa::number::generate_case_3(10, 10);

        for (auto &elem : data)
        {
            std::cout << elem << ",";
        }
        std::cout << std::endl;
    }
    {
        auto data = epa::number::generate_case_4(10);

        for (auto &elem : data)
        {
            std::cout << elem << ",";
        }
        std::cout << std::endl;
    }
    {
        auto data = std::vector<double>(epa::number::generate_case_5(10));

        for (auto &elem : data)
        {
            std::cout << elem << ",";
        }
        std::cout << std::endl;
    }
}
